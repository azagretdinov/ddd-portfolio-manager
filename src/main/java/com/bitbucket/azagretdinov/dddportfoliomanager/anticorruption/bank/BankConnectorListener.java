package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CashAccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CashAccountRequestStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class BankConnectorListener {

    private final BankRepository bankRepository;
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    public BankConnectorListener(
            BankRepository bankRepository, RabbitTemplate rabbitTemplate, ObjectMapper objectMapper
    ) {
        this.bankRepository = bankRepository;
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = objectMapper;
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("bank-event-queue"),
                            exchange = @Exchange(name = "bank", type = "topic"),
                            key = "bank.cash_account.event"
                    )
            }
    )
    public void accountOpened(final AccountOpened accountOpened) {
        final String portfolioReferenceIdentifier = bankRepository.getPortfolioReferenceIdentifier(accountOpened.getRequestId());
        final CashAccountRequestStatus cashAccountRequestStatus = accountOpened.getStatus() == OpenBankAccountResponseStatus.Success ? CashAccountRequestStatus.Success : CashAccountRequestStatus.Failed;
        final CashAccountOpened cashAccountOpened = new CashAccountOpened(portfolioReferenceIdentifier, cashAccountRequestStatus);
        rabbitTemplate.send(
                "bank-anticorruption", "events.cash-account-opened",
                createMessage(cashAccountOpened)
        );
    }

    private Message createMessage(CashAccountOpened cashAccountOpened) {
        return MessageBuilder.withBody(serialise(cashAccountOpened))
                .setHeader("eventType", "cash-account-opened")
                .build();
    }

    private byte[] serialise(CashAccountOpened cashAccountOpened) {
        try {
            return objectMapper.writeValueAsBytes(cashAccountOpened);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
