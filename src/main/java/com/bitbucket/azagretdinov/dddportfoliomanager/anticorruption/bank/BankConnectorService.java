package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.AccountNumber;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.command.OpenCashAccountCommand;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CashAccountDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.util.IdGenerator;

@Component
@Slf4j
public class BankConnectorService {

    private final RabbitTemplate rabbitTemplate;
    private final BankRepository bankRepository;

    public BankConnectorService(RabbitTemplate rabbitTemplate, IdGenerator idGenerator, BankRepository bankRepository) {
        this.rabbitTemplate = rabbitTemplate;
        this.bankRepository = bankRepository;
    }



    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("bank-anticorruption-account-number-queue"),
                            exchange = @Exchange(name = "bank-anticorruption", type = "topic"),
                            key = "account_number"
                    )
            }
    )
    public AccountNumber accountNumber(final String clientReference) {
        return bookAccountNumber(clientReference);
    }

    private AccountNumber bookAccountNumber(String clientReference) {
        return rabbitTemplate.convertSendAndReceiveAsType(
                "bank","bank.account_number", clientReference, new ParameterizedTypeReference<AccountNumber>(){}
        );
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("bank-anticorruption-command-queue"),
                            exchange = @Exchange(name = "bank-anticorruption", type = "topic"),
                            key = "command.open"
                    )
            }
    )
    public void openAccountRequest(OpenCashAccountCommand command){
        sendOpenAccountRequest(command);
    }

    private void sendOpenAccountRequest(OpenCashAccountCommand command) {
        final String requestId = bankRepository.generateRequestId(command.getPortfolioReference());

        CashAccountDetails cashAccountDetails = command.getCashAccountDetails();
        AccountNumber accountNumber = command.getAccountNumber();

        final OpenBankAccountRequest request = OpenBankAccountRequest.builder()
                .requestId(requestId)
                .client(OpenBankAccountRequest.BankAccountClient.builder()
                        .clientIdentifier(cashAccountDetails.getClientReference())
                        .firstName(cashAccountDetails.getFirstName())
                        .lastName(cashAccountDetails.getLastName())
                        .fullName(cashAccountDetails.getFullName())
                        .build())
                .currency(cashAccountDetails.getCurrency())
                .accountNumber(Integer.parseInt(accountNumber.getAccountNumber()))
                .sortCode(Integer.parseInt(accountNumber.getSortCode()))
                .build();

        log.info("Sending OpenBankAccountRequest {}", request);

        rabbitTemplate.convertAndSend(
                "bank", "bank.cash_account.open", request
        );
    }
}
