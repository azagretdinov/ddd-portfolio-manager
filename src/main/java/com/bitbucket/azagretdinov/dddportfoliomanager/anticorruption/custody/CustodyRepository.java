package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.custody;

import org.springframework.stereotype.Repository;
import org.springframework.util.IdGenerator;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class CustodyRepository {

    private final ConcurrentMap<String, String> portfolioReferenceMap;
    private final IdGenerator idGenerator;

    public CustodyRepository(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
        portfolioReferenceMap = new ConcurrentHashMap<>();
    }

    public String generateRequestId(String portfolioReference) {
        final String requestId = idGenerator.generateId().toString();
        portfolioReferenceMap.put(requestId, portfolioReference);
        return requestId;
    }

    public String getPortfolioReferenceIdentifier(String requestId) {
        return portfolioReferenceMap.remove(requestId);
    }
}
