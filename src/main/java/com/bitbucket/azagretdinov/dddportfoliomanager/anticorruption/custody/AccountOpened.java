package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.custody;

import lombok.Builder;
import lombok.Data;

@Data
public class AccountOpened {
    private String requestId;
    private Integer accountNumber;

    public AccountOpened() {

    }

    @Builder
    public AccountOpened(String requestId, Integer accountNumber) {
        this.requestId = requestId;
        this.accountNumber = accountNumber;
    }
}
