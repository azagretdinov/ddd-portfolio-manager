package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank;


import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Country;
import lombok.Builder;
import lombok.Data;

import java.util.Currency;

@Data
public class OpenBankAccountRequest {

    private String requestId;
    private BankAccountClient client;
    private Currency currency;
    private Integer accountNumber;
    private Integer sortCode;

    public OpenBankAccountRequest() {
    }

    @Builder
    public OpenBankAccountRequest(String requestId, BankAccountClient client, Currency currency, Integer accountNumber, Integer sortCode) {
        this.requestId = requestId;
        this.client = client;
        this.currency = currency;
        this.accountNumber = accountNumber;
        this.sortCode = sortCode;
    }

    @Data
    public static class BankAccountClient {
        private String clientIdentifier;
        private String firstName;
        private String lastName;
        private String fullName;
        private Country nationality;

        public BankAccountClient() {
        }

        @Builder
        public BankAccountClient(String clientIdentifier, String firstName, String lastName, String fullName, Country nationality) {
            this.clientIdentifier = clientIdentifier;
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = fullName;
            this.nationality = nationality;
        }
    }
}
