package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Currency;
import java.util.Objects;

@Getter
@Setter
public class AccountOpened {
    private String requestId;
    private OpenBankAccountResponseStatus status;
    private Currency currency;
    private Integer accountNumber;
    private Integer sortCode;

    public AccountOpened() {
    }

    @Builder
    public AccountOpened(String requestId, OpenBankAccountResponseStatus status) {
        this.requestId = requestId;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountOpened that = (AccountOpened) o;
        return Objects.equals(requestId, that.requestId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId);
    }
}
