package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.custody;

import com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank.BankRepository;
import com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank.OpenBankAccountRequest;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.command.OpenCashAccountCommand;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.command.OpenCustodyAccountCommand;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.AccountNumber;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CashAccountDetails;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CustodyAccountDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.util.IdGenerator;

@Component
@Slf4j
public class CustodyConnectorService {

    private final RabbitTemplate rabbitTemplate;
    private final CustodyRepository custodyRepository;

    public CustodyConnectorService(RabbitTemplate rabbitTemplate, CustodyRepository custodyRepository) {
        this.rabbitTemplate = rabbitTemplate;
        this.custodyRepository = custodyRepository;
    }

    private AccountNumber bookAccountNumber(String clientReference) {
        return rabbitTemplate.convertSendAndReceiveAsType(
                "bank","bank.account_number", clientReference, new ParameterizedTypeReference<AccountNumber>(){}
        );
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("custody-anticorruption-command-queue"),
                            exchange = @Exchange(name = "custody-anticorruption", type = "topic"),
                            key = "command.open"
                    )
            }
    )
    public void openAccountRequest(OpenCustodyAccountCommand command){
        sendOpenAccountRequest(command);
    }

    private void sendOpenAccountRequest(OpenCustodyAccountCommand command) {
        final String requestId = custodyRepository.generateRequestId(command.getPortfolioReference());

        final CustodyAccountDetails custodyAccountDetails = command.getCustodyAccountDetails();
        final OpenCustodyAccountRequest request = OpenCustodyAccountRequest.builder()
                .requestId(requestId)
                .clientDetails(OpenCustodyAccountRequest.ClientDetails.builder()
                        .clientIdentifier(custodyAccountDetails.getClientReference())
                        .firstName(custodyAccountDetails.getFirstName())
                        .lastName(custodyAccountDetails.getLastName())
                        .fullName(custodyAccountDetails.getFullName())
                        .build())
                .build();

        log.info("Sending OpenCustodyAccountRequest {}", request);

        rabbitTemplate.convertAndSend(
                "custody", "custody.open-account", request
        );
    }
}
