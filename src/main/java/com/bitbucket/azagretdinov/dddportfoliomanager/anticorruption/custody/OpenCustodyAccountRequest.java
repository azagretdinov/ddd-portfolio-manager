package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.custody;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Country;
import lombok.Builder;
import lombok.Data;

@Data
public class OpenCustodyAccountRequest {
    private String requestId;
    private ClientDetails clientDetails;

    public OpenCustodyAccountRequest(){

    }

    @Builder
    public OpenCustodyAccountRequest(String requestId, ClientDetails clientDetails) {
        this.requestId = requestId;
        this.clientDetails = clientDetails;
    }

    @Data
    public static class ClientDetails {
        private String clientIdentifier;
        private String firstName;
        private String lastName;
        private String fullName;
        private Country nationality;

        public ClientDetails() {
        }

        @Builder
        public ClientDetails(String clientIdentifier, String firstName, String lastName, String fullName, Country nationality) {
            this.clientIdentifier = clientIdentifier;
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = fullName;
            this.nationality = nationality;
        }
    }
}
