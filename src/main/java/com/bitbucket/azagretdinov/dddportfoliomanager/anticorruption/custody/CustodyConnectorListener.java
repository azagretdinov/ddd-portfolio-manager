package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.custody;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CustodyAccountOpened;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustodyConnectorListener {

    private final CustodyRepository custodyRepository;
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    public CustodyConnectorListener(
            CustodyRepository custodyRepository, RabbitTemplate rabbitTemplate, ObjectMapper objectMapper
    ) {
        this.custodyRepository = custodyRepository;
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = objectMapper;
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("custody-event-queue"),
                            exchange = @Exchange(name = "custody", type = "topic"),
                            key = "event"
                    )
            }
    )
    public void accountOpened(final AccountOpened accountOpened) {
        final String portfolioReferenceIdentifier = custodyRepository.getPortfolioReferenceIdentifier(accountOpened.getRequestId());
        final CustodyAccountOpened custodyAccountOpened = new CustodyAccountOpened(
                portfolioReferenceIdentifier, String.valueOf(accountOpened.getAccountNumber())
        );
        log.info("publish event: {}", custodyAccountOpened);
        rabbitTemplate.send(
                "custody-anticorruption", "events.account-opened",
                createMessage(custodyAccountOpened)
        );
    }

    private Message createMessage(CustodyAccountOpened custodyAccountOpened) {
        return MessageBuilder.withBody(serialise(custodyAccountOpened))
                .setHeader("eventType", "custody-account-opened")
                .build();
    }

    private byte[] serialise(CustodyAccountOpened custodyAccountOpened) {
        try {
            return objectMapper.writeValueAsBytes(custodyAccountOpened);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
