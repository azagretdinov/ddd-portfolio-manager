package com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank;

public enum OpenBankAccountResponseStatus {
    Success,
    Failed
}
