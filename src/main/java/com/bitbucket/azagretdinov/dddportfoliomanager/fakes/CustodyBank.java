package com.bitbucket.azagretdinov.dddportfoliomanager.fakes;

import com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.custody.AccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.custody.OpenCustodyAccountRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
public class CustodyBank {

    private final AtomicInteger accountCounter;
    private final BlockingQueue<OpenCustodyAccountRequest> requests;
    private final RabbitTemplate rabbitTemplate;

    public CustodyBank(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        accountCounter = new AtomicInteger(10000000);
        requests = new ArrayBlockingQueue<>(1000);
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("custody-open-account-queue"),
                            exchange = @Exchange(name = "custody", type = "topic"),
                            key = "custody.open-account"
                    )
            }
    )
    public void openAccount(final OpenCustodyAccountRequest request) {
        log.info("Receiver request={}", request);
        requests.add(request);
    }

    @Scheduled(fixedRateString = "PT40S")
    public void openNewAccount() {
        log.info("custody batch triggered");
        final List<OpenCustodyAccountRequest> newRequests = new ArrayList<>();
        requests.drainTo(newRequests);

        newRequests.forEach(this::createAccount);
    }

    private void createAccount(OpenCustodyAccountRequest request) {
        Integer accountNumber = accountCounter.getAndIncrement();
        log.info("New account created: {}", accountNumber);

        rabbitTemplate.convertAndSend(
                "custody", "event",
                AccountOpened.builder()
                        .requestId(request.getRequestId())
                        .accountNumber(accountNumber)
                        .build()
        );
    }
}
