package com.bitbucket.azagretdinov.dddportfoliomanager.fakes;

import com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank.AccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank.OpenBankAccountRequest;
import com.bitbucket.azagretdinov.dddportfoliomanager.anticorruption.bank.OpenBankAccountResponseStatus;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.AccountNumber;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
public class Bank {

    private final AtomicInteger accountCounter;
    private final BlockingQueue<OpenBankAccountRequest> requests;
    private final RabbitTemplate rabbitTemplate;
    private final ConcurrentMap<String, Account> accounts;

    public Bank(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        accountCounter = new AtomicInteger(100000);
        requests = new ArrayBlockingQueue<>(1000);
        accounts = new ConcurrentHashMap<>();
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("bank-account-number-queue"),
                            exchange = @Exchange(name = "bank", type = "topic"),
                            key = "bank.account_number"
                    )
            }
    )
    public AccountNumber accountNumber(final String clientReference) {
        return AccountNumber.builder()
                .sortCode("1000001")
                .accountNumber(Objects.toString(accountCounter.getAndIncrement()))
                .swiftCode("AAUK")
                .build();
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("bank-command-queue"),
                            exchange = @Exchange(name = "bank", type = "topic"),
                            key = "bank.cash_account.open"
                    )
            }
    )
    public void openAccount(final OpenBankAccountRequest request) {
        log.info("Receiver request={}", request);
        requests.add(request);
    }

    @Scheduled(fixedRateString = "PT20S")
    public void openNewAccount() {
        log.info("batch triggered");
        final List<OpenBankAccountRequest> newRequests = new ArrayList<>();
        requests.drainTo(newRequests);

        newRequests.forEach(this::createAccount);
    }

    private void createAccount(OpenBankAccountRequest request) {
        final Account account = new Account(request.getAccountNumber(), request.getSortCode(), request.getCurrency());
        accounts.put(account.getKey(), account);

        log.info("New account created: {}", account);

        rabbitTemplate.convertAndSend(
                "bank", "bank.cash_account.event",
                AccountOpened.builder()
                        .requestId(request.getRequestId())
                        .status(OpenBankAccountResponseStatus.Success)

                        .build()
        );
    }


    @Data
    private static final class Account {
        private final Integer accountNumber;
        private final Integer sortCode;
        private final Currency currency;
        private final double balance;

        private Account(Integer accountNumber, Integer sortCode, Currency currency) {
            this.accountNumber = accountNumber;
            this.sortCode = sortCode;
            this.currency = currency;
            this.balance = 0.0;
        }

        public String getKey() {
            return accountNumber + "-" + sortCode;
        }
    }
}
