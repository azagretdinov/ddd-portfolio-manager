package com.bitbucket.azagretdinov.dddportfoliomanager;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service.PortfolioService;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.view.PortfolioView;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.ApplicationForm;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Portfolio;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.util.IdGenerator;
import org.springframework.util.JdkIdGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.net.URI;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@EnableRabbit
@EnableScheduling
@SpringBootApplication
public class DddPortfolioManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DddPortfolioManagerApplication.class, args);
    }

    @Controller
    public static class API {

        private final PortfolioService portfolioService;

        public API(PortfolioService portfolioService) {
            this.portfolioService = portfolioService;
        }

        @GetMapping("/portfolio/{reference_number}")
        public ResponseEntity<PortfolioView> getPortfolio(@PathVariable("reference_number") String referenceNumber) {
            return ResponseEntity.ok(portfolioService.findPortfolioByReferenceNumber(referenceNumber));
        }

        @PostMapping(
                path = "/portfolio",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE
        )
        public ResponseEntity<Void> openPortfolio(@RequestBody final ApplicationForm applicationForm) {
            final Portfolio portfolio = portfolioService.openPortfolio(applicationForm);
            final URI link = linkTo(ControllerLinkBuilder.methodOn(API.class).getPortfolio(portfolio.getReferenceIdentifier())).toUri();
            return ResponseEntity.created(link).build();
        }
    }


    @Bean
    public IdGenerator idGenerator() {
        return new JdkIdGenerator();
    }

    @Bean
    public Jackson2JsonMessageConverter jackson2JsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }


    @Bean
    public TopicExchange portfolioEventsFanoutExchange(){
        return new TopicExchange("portfolio-events", true, false);
    }
}
