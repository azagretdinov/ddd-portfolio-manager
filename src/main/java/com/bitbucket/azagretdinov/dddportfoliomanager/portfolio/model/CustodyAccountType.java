package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

public enum CustodyAccountType {
    Equity,
    Bonds
}
