package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event;

import lombok.Data;

@Data
public class PortfolioApplicationFormReceived implements PortfolioEvent {
    private String referenceIdentifier;

    public PortfolioApplicationFormReceived(String referenceIdentifier) {
        this.referenceIdentifier = referenceIdentifier;
    }
}
