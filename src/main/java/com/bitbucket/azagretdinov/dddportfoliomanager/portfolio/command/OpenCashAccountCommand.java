package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.command;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.AccountNumber;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CashAccountDetails;
import lombok.Data;

@Data
public class OpenCashAccountCommand {
    private String portfolioReference;
    private AccountNumber accountNumber;
    private CashAccountDetails cashAccountDetails;

    public OpenCashAccountCommand(){

    }

    public OpenCashAccountCommand(String portfolioReference, AccountNumber accountNumber, CashAccountDetails cashAccountDetails) {
        this.portfolioReference = portfolioReference;
        this.accountNumber = accountNumber;
        this.cashAccountDetails = cashAccountDetails;
    }
}
