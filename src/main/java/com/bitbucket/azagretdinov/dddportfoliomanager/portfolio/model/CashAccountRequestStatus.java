package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

public enum CashAccountRequestStatus {
    Failed, Success
}
