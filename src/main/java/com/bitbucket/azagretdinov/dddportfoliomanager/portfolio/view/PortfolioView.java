package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.view;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.AccountNumber;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CashAccount;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.ClientRiskRate;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CustodyAccount;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.PortfolioRiskRate;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.PortfolioRiskRateCalculator;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.PortfolioStatus;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.PortfolioType;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.TaskType;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Currency;
import java.util.Set;

@Builder
@Getter
public class PortfolioView {

    private final String referenceIdentifier;

    private final Set<TaskView> pendingTasks;

    private final String clientRef;
    private final String type;

    private String riskRate;
    private String clientRiskRate;

    private String cashAccountNumber;
    private String internationalBankAccountNumber;
    private Currency cashAccountCurrency;

    private String custodyAccountNumber;

    private String status;

    private LocalTime openTime;
    private LocalDate openDate;

}
