package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository.support;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository.PortfolioRepository;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Portfolio;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository.PortfolioViewRepository;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class InMemoryPortfolioRepository implements PortfolioRepository {

    private final PortfolioViewRepository portfolioViewRepository;

    private final ConcurrentMap<String, Portfolio> data;

    public InMemoryPortfolioRepository(PortfolioViewRepository portfolioViewRepository) {
        this.portfolioViewRepository = portfolioViewRepository;
        data = new ConcurrentHashMap<>();
    }

    @Override
    public void store(Portfolio portfolio) {
        data.put(portfolio.getReferenceIdentifier(), portfolio);
        portfolioViewRepository.store(portfolio);
    }

    @Override
    public Portfolio get(final String referenceNumber) {
        return data.get(referenceNumber);
    }
}
