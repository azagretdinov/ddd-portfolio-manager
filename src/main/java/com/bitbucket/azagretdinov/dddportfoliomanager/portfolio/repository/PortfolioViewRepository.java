package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Portfolio;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.view.PortfolioView;

import java.util.List;

public interface PortfolioViewRepository {
    void store(Portfolio portfolio);

    PortfolioView findPortfolioByReferenceNumber(String referenceNumber);

    List<PortfolioView> findAllPortfolioByClientRef(String clientReferenceNumber);
}
