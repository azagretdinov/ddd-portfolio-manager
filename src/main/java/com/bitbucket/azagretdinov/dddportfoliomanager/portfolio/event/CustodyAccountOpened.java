package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event;

import lombok.Data;

@Data
public class CustodyAccountOpened {
    private String portfolioReference;
    private String accountNumber;

    public CustodyAccountOpened() {
    }

    public CustodyAccountOpened(String portfolioReference, String accountNumber) {
        this.portfolioReference = portfolioReference;
        this.accountNumber = accountNumber;
    }
}
