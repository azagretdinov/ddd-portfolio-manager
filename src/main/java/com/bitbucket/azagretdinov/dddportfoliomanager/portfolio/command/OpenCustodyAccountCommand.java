package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.command;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CustodyAccountDetails;
import lombok.Data;

@Data
public class OpenCustodyAccountCommand {
    private String portfolioReference;
    private CustodyAccountDetails custodyAccountDetails;

    public OpenCustodyAccountCommand(){

    }

    public OpenCustodyAccountCommand(String portfolioReference, CustodyAccountDetails custodyAccountDetails) {
        this.portfolioReference = portfolioReference;
        this.custodyAccountDetails = custodyAccountDetails;
    }
}
