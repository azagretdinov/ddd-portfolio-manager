package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service.support;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.*;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.messaging.PortfolioEventPublisher;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.ApplicationForm;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Portfolio;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.PortfolioFactory;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository.PortfolioRepository;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository.PortfolioViewRepository;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service.PortfolioService;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.view.PortfolioView;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimplePortfolioService implements PortfolioService {

    private final PortfolioFactory portfolioFactory;

    private final PortfolioRepository portfolioRepository;
    private final PortfolioViewRepository portfolioViewRepository;
    private final PortfolioEventPublisher portfolioEventPublisher;

    public SimplePortfolioService(
            PortfolioFactory portfolioFactory,
            PortfolioRepository portfolioRepository,
            PortfolioViewRepository portfolioViewRepository,
            PortfolioEventPublisher portfolioEventPublisher) {
        this.portfolioFactory = portfolioFactory;
        this.portfolioRepository = portfolioRepository;
        this.portfolioViewRepository = portfolioViewRepository;
        this.portfolioEventPublisher = portfolioEventPublisher;
    }

    @Override
    public Portfolio openPortfolio(ApplicationForm applicationForm){

        final Portfolio portfolio = portfolioFactory.openPortfolio(applicationForm);
        portfolioRepository.store(portfolio);
        publish(new PortfolioApplicationFormReceived(portfolio.getReferenceIdentifier()));

        return portfolio;
    }

    @Override
    public PortfolioView findPortfolioByReferenceNumber(String referenceNumber) {
        return portfolioViewRepository.findPortfolioByReferenceNumber(referenceNumber);
    }

    @Override
    public List<PortfolioView> findAllPortfolioByClientRef(String clientReferenceNumber) {
        return portfolioViewRepository.findAllPortfolioByClientRef(clientReferenceNumber);
    }

    @Override
    public void handleEvent(CashAccountOpened cashAccountOpened) {
        final Portfolio portfolio = portfolioRepository.get(cashAccountOpened.getPortfolioReference());
        final PortfolioEvent event = portfolio.apply(cashAccountOpened);
        portfolioRepository.store(portfolio);
        publish(event);
    }

    @Override
    public void handleEvent(CustodyAccountOpened custodyAccountOpened) {
        final Portfolio portfolio = portfolioRepository.get(custodyAccountOpened.getPortfolioReference());
        final PortfolioEvent event = portfolio.apply(custodyAccountOpened);
        portfolioRepository.store(portfolio);
        publish(event);
    }

    private void publish(PortfolioEvent event) {
        if (event != null) {
            portfolioEventPublisher.publish(determinateType(event), event);
        }
    }

    private String determinateType(PortfolioEvent event) {
        if(event instanceof PortfolioOpened){
            return "portfolio-opened";
        }else if(event instanceof PortfolioOpenProcessUpdated){
            return "portfolio-open-process-updated";
        }else if(event instanceof PortfolioApplicationFormReceived){
            return "portfolio-application-form-received";
        }
        throw new IllegalArgumentException("Unknown event " + event);
    }
}
