package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository.support;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.AccountNumber;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CashAccount;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Portfolio;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository.PortfolioViewRepository;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.view.PortfolioView;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class InMemoryPortfolioViewRepository implements PortfolioViewRepository {


    private final ConcurrentMap<String, PortfolioView> data;
    private final ConcurrentMap<String, List<PortfolioView>> clientPortfolios;

    public InMemoryPortfolioViewRepository() {
        data = new ConcurrentHashMap<>();
        clientPortfolios = new ConcurrentHashMap<>();
    }

    @Override
    public void store(Portfolio portfolio) {
        final PortfolioView portfolioView = create(portfolio);
        data.put(portfolio.getReferenceIdentifier(), portfolioView);
        clientPortfolios.computeIfAbsent(portfolio.getClientRef(), k -> new ArrayList<>()).add(portfolioView);
    }

    private PortfolioView create(Portfolio portfolio) {
        return PortfolioView.builder()
                .referenceIdentifier(portfolio.getReferenceIdentifier())
                .clientRef(portfolio.getClientRef())
                .cashAccountNumber(format(portfolio.getCashAccount()))
                .internationalBankAccountNumber(internationalFormat(portfolio.getCashAccount()))
                .cashAccountCurrency(portfolio.getCashAccount().getCurrency())
                .custodyAccountNumber(portfolio.getCustodyAccount().getAccountNumber())
                .openDate(portfolio.getOpenDate())
                .openTime(portfolio.getOpenTime())
                .build();
    }

    private String internationalFormat(CashAccount cashAccount) {
        if (cashAccount == null){
            return "";
        }
        if(cashAccount.getAccountNumber() == null){
            return "";
        }
        return String.format(
                "UK%s%s%s%s",
                checkSum(cashAccount.getAccountNumber()),
                cashAccount.getAccountNumber().getSwiftCode(),
                cashAccount.getAccountNumber().getSortCode(),
                cashAccount.getAccountNumber().getAccountNumber()
        );
    }

    private int checkSum(AccountNumber accountNumber) {
        return 16;
    }

    private String format(CashAccount cashAccount) {
        if (cashAccount == null){
            return "";
        }
        if(cashAccount.getAccountNumber() == null){
            return "";
        }
        return String.format("%s - %s", cashAccount.getAccountNumber().getSortCode(), cashAccount.getAccountNumber().getAccountNumber());
    }

    @Override
    public PortfolioView findPortfolioByReferenceNumber(String referenceNumber) {
        return data.get(referenceNumber);
    }

    @Override
    public List<PortfolioView> findAllPortfolioByClientRef(String clientReferenceNumber) {
        return clientPortfolios.getOrDefault(clientReferenceNumber, Collections.emptyList());
    }
}
