package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.PortfolioEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

@Configuration
@Slf4j
public class PortfolioEventPublisher{
    public static final String EVENTS_EXCHANGE = "portfolio-events";

    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    public PortfolioEventPublisher(RabbitTemplate rabbitTemplate, ObjectMapper objectMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = objectMapper;
    }

    public void publish(String eventType, PortfolioEvent portfolioEvent){
        log.info("Publishing event {}", portfolioEvent);
        rabbitTemplate.send(
                EVENTS_EXCHANGE,
                "#",
                createMessage(eventType,portfolioEvent)
        );
    }

    private Message createMessage(String eventType, PortfolioEvent portfolioEvent){
            return MessageBuilder.withBody(serialise(portfolioEvent))
                    .setContentType(MediaType.APPLICATION_JSON_VALUE)
                    .setHeader("eventType", eventType)
                    .build();
    }

    private byte[] serialise(PortfolioEvent portfolioEvent) {
        try {
            return objectMapper.writeValueAsBytes(portfolioEvent);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
