package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.messaging;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CashAccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CustodyAccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service.PortfolioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class PortfolioManagementListeners {
    private final ObjectMapper objectMapper;
    private final PortfolioService portfolioService;

    public PortfolioManagementListeners(ObjectMapper objectMapper, PortfolioService portfolioService) {
        this.objectMapper = objectMapper;
        this.portfolioService = portfolioService;
    }

    @RabbitListener(
            bindings = {
                    @QueueBinding(
                            value = @Queue("portfolio-management-in-queue"),
                            exchange = @Exchange(name = "bank-anticorruption", type = "topic"),
                            key = "events.*"
                    ),
                    @QueueBinding(
                            value = @Queue("portfolio-management-in-queue"),
                            exchange = @Exchange(name = "custody-anticorruption", type = "topic"),
                            key = "events.*"
                    )
            }
    )
    public void onEvent(@Header("eventType") String eventType, @Payload byte[] payload) {
        if ("cash-account-opened".equals(eventType)) {
            final CashAccountOpened cashAccountOpened = deserialize(payload, CashAccountOpened.class);
            portfolioService.handleEvent(cashAccountOpened);
        }else if ("custody-account-opened".equals(eventType)) {
            final CustodyAccountOpened custodyAccountOpened = deserialize(payload, CustodyAccountOpened.class);
            portfolioService.handleEvent(custodyAccountOpened);
        }
    }


    private <T> T deserialize(byte[] payload, Class<T> klass) {
        try {
            return objectMapper.readValue(payload, klass);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

}
