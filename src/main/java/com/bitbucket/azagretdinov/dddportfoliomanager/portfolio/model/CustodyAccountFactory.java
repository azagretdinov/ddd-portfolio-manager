package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.command.OpenCustodyAccountCommand;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class CustodyAccountFactory {
    private final RabbitTemplate rabbitTemplate;

    public CustodyAccountFactory(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }


    public CustodyAccount openCustodyAccount(String portfolioReferenceIdentifier, CustodyAccountDetails custodyAccountDetails) {
        final CustodyAccount custodyAccount = new CustodyAccount();
        rabbitTemplate.convertAndSend(
                "custody-anticorruption", "command.open",
                new OpenCustodyAccountCommand(
                        portfolioReferenceIdentifier, custodyAccountDetails
                )
        );

        return custodyAccount;
    }
}
