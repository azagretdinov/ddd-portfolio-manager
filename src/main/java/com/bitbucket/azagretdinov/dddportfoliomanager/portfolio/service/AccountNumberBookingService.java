package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.AccountNumber;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

@Component
public class AccountNumberBookingService {

    private final RabbitTemplate rabbitTemplate;

    public AccountNumberBookingService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public AccountNumber bookAccountNumber(String clientReference) {
        return rabbitTemplate.convertSendAndReceiveAsType(
                "bank-anticorruption","account_number", clientReference, new ParameterizedTypeReference<AccountNumber>(){}
                );
    }
}
