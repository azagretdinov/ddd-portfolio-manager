package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.repository;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Portfolio;

public interface PortfolioRepository {
    void store(Portfolio portfolio);

    Portfolio get(String referenceNumber);
}
