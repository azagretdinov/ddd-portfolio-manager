package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CashAccountOpened;
import lombok.Getter;
import lombok.Setter;

import java.util.Currency;

@Setter
@Getter
public class CashAccount {
    private CashAccountStatus status;
    private String bankRequestReference;
    private AccountNumber accountNumber;
    private Currency currency;

    public CashAccount() {
        this.status = CashAccountStatus.Pending;
    }

    public void apply(CashAccountOpened cashAccountOpened) {
        if(cashAccountOpened.getCashAccountRequestStatus() == CashAccountRequestStatus.Success){
            status = CashAccountStatus.Opened;
        }
    }

    public boolean isReady() {
        return status == CashAccountStatus.Opened;
    }
}
