package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import lombok.Data;

import java.util.Currency;

@Data
public class CashAccountDetails {
    private String clientReference;
    private Currency currency;
    private String firstName;
    private String lastName;
    private String fullName;
}
