package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service.AccountNumberBookingService;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.command.OpenCashAccountCommand;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class CashAccountFactory {

    private final AccountNumberBookingService accountNumberBookingService;
    private final RabbitTemplate rabbitTemplate;


    public CashAccountFactory(AccountNumberBookingService accountNumberBookingService, RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        this.accountNumberBookingService = accountNumberBookingService;
    }

    public CashAccount openCashAccount(String portfolioReferenceIdentifier, CashAccountDetails cashAccountDetails) {

        final AccountNumber accountNumber = accountNumberBookingService.bookAccountNumber(cashAccountDetails.getClientReference());

        final CashAccount cashAccount = new CashAccount();

        cashAccount.setAccountNumber(accountNumber);
        cashAccount.setCurrency(cashAccountDetails.getCurrency());

        rabbitTemplate.convertAndSend(
                "bank-anticorruption", "command.open",
                new OpenCashAccountCommand(portfolioReferenceIdentifier, accountNumber, cashAccountDetails)
        );

        return cashAccount;
    }

}
