package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event;

public interface PortfolioEvent {
    String getReferenceIdentifier();

    void setReferenceIdentifier(String referenceIdentifier);
}
