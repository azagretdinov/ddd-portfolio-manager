package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

public enum  TaskType {
    CustodyAccount, CashAccount
}
