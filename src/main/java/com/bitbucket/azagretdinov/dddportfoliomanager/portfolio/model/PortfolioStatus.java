package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

public enum PortfolioStatus {
    Pending,
    Opened
}
