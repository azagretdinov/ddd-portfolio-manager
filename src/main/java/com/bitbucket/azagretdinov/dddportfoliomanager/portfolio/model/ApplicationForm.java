package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import lombok.Data;

import java.util.Objects;

@Data
public class ApplicationForm {
    private String applicationId;
    private String clientReference;
    private ClientRiskRate clientRiskRate;
    private PortfolioType portfolioType;
    private CashAccountDetails cashAccountDetails;
    private CustodyAccountDetails custodyAccountDetails;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApplicationForm that = (ApplicationForm) o;
        return applicationId.equals(that.applicationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applicationId);
    }
}
