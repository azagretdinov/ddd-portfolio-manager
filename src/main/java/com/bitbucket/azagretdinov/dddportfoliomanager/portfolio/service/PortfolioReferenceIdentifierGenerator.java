package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class PortfolioReferenceIdentifierGenerator {

    private final AtomicInteger dayCounter;

    public PortfolioReferenceIdentifierGenerator() {
        dayCounter = new AtomicInteger(1);
    }

    public String generate(String clientRef) {
        return clientRef + "-" + LocalDate.now() + "-" + dayCounter.getAndIncrement();
    }
}
