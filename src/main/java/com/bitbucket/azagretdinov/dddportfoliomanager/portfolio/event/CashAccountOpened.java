package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.CashAccountRequestStatus;
import lombok.Data;

@Data
public class CashAccountOpened {
    private String portfolioReference;
    private CashAccountRequestStatus cashAccountRequestStatus;

    public CashAccountOpened() {
    }

    public CashAccountOpened(String portfolioReference, CashAccountRequestStatus cashAccountRequestStatus) {
        this.portfolioReference = portfolioReference;
        this.cashAccountRequestStatus = cashAccountRequestStatus;
    }
}
