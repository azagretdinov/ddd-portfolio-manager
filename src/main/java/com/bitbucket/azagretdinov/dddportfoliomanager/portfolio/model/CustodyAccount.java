package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import lombok.Data;

@Data
public class CustodyAccount {
    private CustodyAccountStatus status;
    private String accountNumber;

    public CustodyAccount() {
        this.status = CustodyAccountStatus.Pending;
    }
}
