package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

public enum CashAccountStatus {
    Pending,
    Opened
}
