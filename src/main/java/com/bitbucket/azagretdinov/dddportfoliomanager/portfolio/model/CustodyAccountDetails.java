package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import lombok.Data;

import java.util.Currency;

@Data
public class CustodyAccountDetails {
    private String clientReference;
    private String firstName;
    private String lastName;
    private String fullName;
    private CustodyAccountType type;
}
