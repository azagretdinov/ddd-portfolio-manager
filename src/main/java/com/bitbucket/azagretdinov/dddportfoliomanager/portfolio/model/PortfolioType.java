package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

public enum PortfolioType {
    Currency,
    Equity,
    Bonds
}
