package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service.PortfolioReferenceIdentifierGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PortfolioFactory {
    private final PortfolioRiskRateCalculator portfolioRiskRateCalculator;
    private final PortfolioReferenceIdentifierGenerator portfolioReferenceIdentifierGenerator;
    private final CashAccountFactory cashAccountFactory;
    private final CustodyAccountFactory custodyAccountFactory;

    public PortfolioFactory(
            PortfolioRiskRateCalculator portfolioRiskRateCalculator,
            PortfolioReferenceIdentifierGenerator portfolioReferenceIdentifierGenerator,
            CashAccountFactory cashAccountFactory, CustodyAccountFactory custodyAccountFactory) {
        this.portfolioRiskRateCalculator = portfolioRiskRateCalculator;
        this.portfolioReferenceIdentifierGenerator = portfolioReferenceIdentifierGenerator;
        this.cashAccountFactory = cashAccountFactory;
        this.custodyAccountFactory = custodyAccountFactory;
    }


    public Portfolio openPortfolio(ApplicationForm applicationForm) {
        final String portfolioReferenceIdentifier = portfolioReferenceIdentifierGenerator.generate(applicationForm.getClientReference());

        final Portfolio portfolio = new Portfolio(
                portfolioReferenceIdentifier,
                applicationForm.getClientReference(),
                portfolioRiskRateCalculator,
                applicationForm.getPortfolioType()
        );

        portfolio.setClientRiskRate(applicationForm.getClientRiskRate());
        portfolio.addTask(
                TaskType.CashAccount,
                TaskType.CustodyAccount
                );

        final CashAccountDetails cashAccountDetails = applicationForm.getCashAccountDetails();

        portfolio.setCashAccount(cashAccountFactory.openCashAccount(portfolioReferenceIdentifier, cashAccountDetails));
        portfolio.setCustodyAccount(custodyAccountFactory.openCustodyAccount(portfolioReferenceIdentifier, applicationForm.getCustodyAccountDetails()));

        log.info("Portfolio instance created. portfolioReferenceIdentifier={}", portfolioReferenceIdentifier);

        return portfolio;
    }
}
