package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.service;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CashAccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CustodyAccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.ApplicationForm;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.Portfolio;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.view.PortfolioView;

import java.util.List;

public interface PortfolioService {
    Portfolio openPortfolio(ApplicationForm applicationForm);

    PortfolioView findPortfolioByReferenceNumber(String referenceNumber);

    List<PortfolioView> findAllPortfolioByClientRef(String referenceNumber);

    void handleEvent(CashAccountOpened cashAccountOpened);

    void handleEvent(CustodyAccountOpened custodyAccountOpened);
}
