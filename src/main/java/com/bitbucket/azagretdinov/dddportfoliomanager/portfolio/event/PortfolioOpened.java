package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.*;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
public class PortfolioOpened implements PortfolioEvent {

    private LocalDate openDate;
    private LocalTime openTime;

    private String referenceIdentifier;
    private String clientRef;
    private PortfolioType type;

    private PortfolioRiskRate riskRate;
    private ClientRiskRate clientRiskRate;

    private AccountNumber cashAccountNumber;
    private String custodyAccountNumber;

    private PortfolioStatus status;
}
