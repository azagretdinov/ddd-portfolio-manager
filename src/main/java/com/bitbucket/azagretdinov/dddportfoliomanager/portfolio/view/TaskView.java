package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.view;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Builder
@Getter
public class TaskView {
    final String taskName;
}
