package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import lombok.Builder;
import lombok.Data;


@Data
public class AccountNumber {
    private String sortCode;
    private String accountNumber;
    private String swiftCode;

    public AccountNumber(){

    }

    @Builder
    public AccountNumber(String sortCode, String accountNumber, String swiftCode) {
        this.sortCode = sortCode;
        this.accountNumber = accountNumber;
        this.swiftCode = swiftCode;
    }
}
