package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model.TaskType;
import lombok.Data;

import java.util.List;

@Data
public class PortfolioOpenProcessUpdated implements PortfolioEvent {
    private String referenceIdentifier;
    private List<TaskType> pendingTasks;

    public PortfolioOpenProcessUpdated(String referenceIdentifier, List<TaskType> pendingTasks) {
        this.referenceIdentifier = referenceIdentifier;
        this.pendingTasks = pendingTasks;
    }
}
