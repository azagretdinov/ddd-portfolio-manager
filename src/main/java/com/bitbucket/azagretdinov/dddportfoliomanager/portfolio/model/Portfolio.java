package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CashAccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.CustodyAccountOpened;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.PortfolioEvent;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.PortfolioOpenProcessUpdated;
import com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.event.PortfolioOpened;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Slf4j
public class Portfolio {
    private final String referenceIdentifier;

    private final Set<TaskType> pendingTasks;

    private final String clientRef;
    private final PortfolioRiskRateCalculator portfolioRiskRateCalculator;
    private final PortfolioType type;

    private PortfolioRiskRate riskRate;
    private ClientRiskRate clientRiskRate;
    private CashAccount cashAccount;
    private CustodyAccount custodyAccount;
    private PortfolioStatus status;

    private LocalTime openTime;
    private LocalDate openDate;

    public Portfolio(String referenceIdentifier, String clientRef, PortfolioRiskRateCalculator portfolioRiskRateCalculator, PortfolioType type) {
        this.referenceIdentifier = referenceIdentifier;
        this.clientRef = clientRef;
        this.portfolioRiskRateCalculator = portfolioRiskRateCalculator;
        this.type = type;
        pendingTasks = new LinkedHashSet<>();
        status = PortfolioStatus.Pending;

    }

    public void addTask(TaskType... tasks) {
        pendingTasks.addAll(Arrays.asList(tasks));
    }

    public PortfolioEvent apply(CashAccountOpened cashAccountOpened) {
        cashAccount.apply(cashAccountOpened);
        if (cashAccount.isReady()) {
            pendingTasks.remove(TaskType.CashAccount);
        }
        return recalculateReadiness();
    }

    public PortfolioEvent apply(CustodyAccountOpened custodyAccountOpened) {
        custodyAccount.setAccountNumber(custodyAccountOpened.getAccountNumber());
        pendingTasks.remove(TaskType.CustodyAccount);
        return recalculateReadiness();
    }

    private PortfolioEvent recalculateReadiness() {
        if (isReady()) {
            finishOpeningPortfolio();
            return PortfolioOpened.builder()
                    .openTime(openTime)
                    .openDate(openDate)
                    .referenceIdentifier(referenceIdentifier)
                    .cashAccountNumber(cashAccount.getAccountNumber())
                    .custodyAccountNumber(custodyAccount.getAccountNumber())
                    .riskRate(riskRate)
                    .clientRiskRate(clientRiskRate)
                    .clientRef(clientRef)
                    .status(status)
                    .type(type)
                    .build();
        }
        return new PortfolioOpenProcessUpdated(
                referenceIdentifier,
                new ArrayList<>(pendingTasks)
        );
    }

    private void finishOpeningPortfolio() {
        status = PortfolioStatus.Opened;
        openTime = LocalTime.now();
        openDate = LocalDate.now();
        riskRate = portfolioRiskRateCalculator.calculatePortfolioRiskRate(clientRiskRate, type);
        log.info("Portfolio portfolioReferenceIdentifier={} opened with riskRate={}", referenceIdentifier, riskRate);
    }

    private boolean isReady() {
        return pendingTasks.isEmpty();
    }


}
