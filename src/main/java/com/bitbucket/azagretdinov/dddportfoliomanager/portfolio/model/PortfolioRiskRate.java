package com.bitbucket.azagretdinov.dddportfoliomanager.portfolio.model;

public enum PortfolioRiskRate {
    Low,
    High,
    Critical
}
